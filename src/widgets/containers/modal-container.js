import React, { Component } from 'react';
import { createPortal } from 'react-dom';

const portal = document.getElementById("modal-container");

class ModalContainer extends Component{
	render(){
		return createPortal(this.props.children, portal);
	}
}

export default ModalContainer;
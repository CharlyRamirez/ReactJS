import React, { Component } from 'react';
import Search from '../components/search';


class SearchConatiner extends Component{
	state = {
		value: ''
	}
	hundleSubmit = event => {
		event.preventDefault();
		console.log(this.input.value, 'submit');
		//aqui se puede hacer un post para hacer la consulta al servidor
	}
	setInputRef = element => {
		this.input = element;
	}
	hundleInputChange = event => {
		this.setState({
			value: event.target.value.replace(' ', '-')
		})
	}
	render(){
		return(
			<Search
				setRef={this.setInputRef}
				hundleSubmit={this.hundleSubmit}
				hundleChange={this.hundleInputChange}
				value={this.state.value}
			/>
		)
	}
}

export default SearchConatiner;
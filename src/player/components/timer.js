import React from 'react';
import FormattedTime from './formattedtime';

const Timer = (props) => (
	<FormattedTime
		secs={props.currentTime}
		duration={props.duration}
	/>
)

export default Timer;